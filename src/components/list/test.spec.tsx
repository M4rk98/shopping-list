import { render, screen, fireEvent } from '@testing-library/react'

import List from './index'

const mockData = [ { id: 'test item 1', name: 'test item', quantity: 2 } ]
const mockTotal = 2;

describe('list', () => {
  it('renders title & total', () => {
    render(<List title='Test' items={mockData} />)

    const heading = screen.getByRole('heading', { level: 2 })
    const total = screen.getByText(`0 / ${mockTotal}`)

    expect(heading).toBeInTheDocument()
    expect(total).toBeInTheDocument()
  })
  it('renders items', () => {
    render(<List title='Test' items={mockData} />)

    const listItems = screen.getAllByRole('listitem')

    expect(listItems).toHaveLength(mockData.length)
  })
  it('update total finished when list item checked', async () => {
    render(<List title='Test' items={mockData} />)

    const listItem = screen.getByLabelText(mockData[0].name)
    fireEvent.click(listItem);

    const total = await screen.findByText(`${mockData[0].quantity} / ${mockTotal}`)

    expect(total).toBeInTheDocument()
  })
})
