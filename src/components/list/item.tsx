import { Dispatch, SetStateAction, useCallback, useState } from 'react';

import { cn } from '../../utils';
import { ReactComponent as Check } from '../../assets/check.svg';

type Props = Item & {
  setTotalFinished: Dispatch<SetStateAction<number>>;
}

export interface Item {
  id: string;
  name: string;
  quantity: number;
}

function ListItem({ id, name, quantity, setTotalFinished }: Props) {
  const [checked, setChecked] = useState(false);

  const handleChange = useCallback(() => {
    setChecked((prevState) => !prevState)
    if (!checked) {
      setTotalFinished((prevValue) => prevValue + quantity);
    } else {
      setTotalFinished((prevValue) => prevValue - quantity);
    }
  }, [checked, quantity, setTotalFinished])

  return (
    <li className='flex items-center justify-between py-4'>
      <div className='flex items-center gap-x-3 relative cursor-pointer'>
        <div className='relative'>
          <Check onClick={handleChange} className='absolute left-0 right-0 mx-auto top-0 bottom-0 my-auto text-white' />
          <input
            onChange={handleChange}
            checked={checked}
            id={id}
            aria-describedby={`${id} checkbox`}
            name={id}
            type='checkbox'
            className={cn('appearance-none h-[24px] w-[24px] button hover:opacity-100', {
              'bg-blue-400 text-white': checked
            })}
          />
        </div>

        <label htmlFor={id} className={cn('text-midnight font-bold text-base cursor-pointer', {
          'line-through text-altGray font-light': checked
        })}>{name}</label>
      </div>
      <div className='flex text-altGray'>
        <div><p className='sr-only'>Quantity</p>x</div>
        <div className='w-[20px] text-center'>{quantity}</div>
      </div>
    </li>
  );
}

export default ListItem;
