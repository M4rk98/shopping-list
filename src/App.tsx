import React from 'react';
import List from './components/list';
import Header from './components/header'
import Footer from './components/footer';

const shoppingList = [
  { title: 'Vegetables', items: [
      {id: 'Vegetables1', name: 'item 1', quantity: 2 },
      {id: 'Vegetables2', name: 'item 2', quantity: 2 },
      {id: 'Vegetables3', name: 'item 3', quantity: 2 },
      {id: 'Vegetables4', name: 'item 4', quantity: 2 },
      {id: 'Vegetables5', name: 'item 5', quantity: 2 },
      {id: 'Vegetables6', name: 'item 6', quantity: 2 },
      {id: 'Vegetables7', name: 'item 7', quantity: 2 }
    ]
  },
  { title: 'Beverages', items: [
      {id: 'Beverages1', name: 'item 1', quantity: 2 },
      {id: 'Beverages2', name: 'item 2', quantity: 2 },
    ]
  },
  { title: 'Bakery', items: [
      {id: 'Bakery1', name: 'item 1', quantity: 2 },
      {id: 'Bakery2', name: 'item 2', quantity: 2 },
    ]
  }
]

function App() {
  return (
    <div className='max-w-2xl mx-auto px-6 md:px-0'>
      <Header />
      <main className='grid grid-cols-1 xl:grid-cols-2 gap-6'>
        {shoppingList.map((list) => (
          <List key={list.title} title={list.title} items={list.items} />
        ))}
      </main>
      <Footer />
    </div>
  );
}

export default App;
