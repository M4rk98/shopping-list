import Avatar from '../../assets/Avatar.jpg';
import { ReactComponent as Share } from '../../assets/share.svg';

function Header() {
  return (
    <header className='mt-14 text-center flex flex-col items-center'>
      <img src={Avatar} alt='avatar' className='h-[56px] rounded-full' />
      <h1 className='mt-6 font-bold title'>Home</h1>
      <p className='text-altGray mt-1'>Created 2 days ago</p>
      <button type='button' className='button my-6 h-[44px] w-[44px]'>
        <Share className='h-[20px]' />
      </button>
    </header>
  );
}

export default Header;
