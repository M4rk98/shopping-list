## Notes

### Assumptions
- The figma design didn't include an add list item functionality so I used mock data
- For most UI elements I tried to follow the Figma design strictly like in case of the icons, fonts, spacing, etc... but in some cases I decided to use a quick tailwind alternative to be able to stick to the time limit better

### Improvement ideas
- In case of the checkbox component it wasn't obvious for me from the design if some of the states should be implemented for a hover or focus event. I picked the focus event as in my experience it's less common for checkboxes to have hover state but maybe I was wrong in which case this should be improved
- Use icon library like fontawesome and relative units for the icon sizes
- Refactor checkbox to be a separate component and use relative units for its size
- separate page content from JSX code
- SEO improvements - add meta tags, generate sitemap
- Add list item functionality
- Add animation library like Framer Motion


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
