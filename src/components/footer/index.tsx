import Facebook from '../../assets/facebook.svg';
import Twitter from '../../assets/twitter.svg';

function Footer() {
  return (
    <footer className='my-14 text-center'>
      <p className='text-altGray'>Follow us</p>
      <div className='my-3 flex gap-x-3 justify-center'>
        <button type='button' className='button h-[36px] w-[36px]'>
          <img src={Facebook} alt='facebook share' className='h-[20px]' />
        </button>
        <button type='button' className='button h-[36px] w-[36px]'>
          <img src={Twitter} alt='twitter share' className='h-[20px]' />
        </button>
      </div>
    </footer>
  );
}

export default Footer;
