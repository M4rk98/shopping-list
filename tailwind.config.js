const defaultTheme = require('tailwindcss/defaultTheme');

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{js,jsx,ts,tsx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        'sans': ['sf-pro-rounded', ...defaultTheme.fontFamily.sans]
      },
      colors: {
        'midnight': '#171717',
        'altGray': '#A3A3A3',
      }
    },
  },
  plugins: [],
}

