import { useMemo, useState } from 'react';

import ListItem, { Item } from './item';

interface Props {
  title: string;
  items: Item[]
}

function List({ title, items }: Props) {
  const [totalFinished, setTotalFinished] = useState(0);
  const total = useMemo(() => {
    return  items.reduce((accumulator, currentValue) => {
      return accumulator + currentValue.quantity;
    }, 0)
  }, [items])

  return (
    <div>
      <div className='flex justify-between items-center'>
        <h2 className='p-4 text-midnight font-bold'>{title}</h2>
        <p className='text-altGray'>{totalFinished} / {total}</p>
      </div>

      <ul className='bg-white rounded-xl px-4 shadow-xs divide-y divide-gray-100'>
        {items.map((item) => (
            <ListItem key={item.id} id={item.id} name={item.name} quantity={item.quantity} setTotalFinished={setTotalFinished} />
          ))}
      </ul>
    </div>
  );
}

export default List;

